# hospital_final_project

Final project for EPAM (Hospital).

(ENG)

The system administrator has access to a list of doctors by category (pediatrician, traumatologist, surgeon, ...) and a list of patients. Implement the ability to sort:
1) patients:
- alphabetically;
- by date of birth;
2) doctors:
- alphabetically;
- by category;
- by number of patients.
  The administrator registers patients and doctors in the system and appoints a doctor to the patient.
  The doctor determines the diagnosis, makes appointments to the patient (procedures, medications, operations), which are recorded in the Hospital Card. The appointment can be made by a Nurse (procedures, medications) or a Doctor (any appointment).
  The patient can be discharged from the hospital, with a definitive diagnosis recorded.
  (Optional: implement the ability to save / export a document with information about the patient's discharge).


(UA)

Адміністратору системи доступний список Лікарів за категоріями (педіатр, травматолог, хірург, ...) і список Пацієнтів. Реалізувати можливість сортування:
1) пацієнтів:
- за алфавітом;
- за датою народження;
2) лікарів:
- за алфавітом;
- за категорією;
- за кількістю пацієнтів.
  Адміністратор реєструє в системі пацієнтів і лікарів і призначає пацієнтові лікаря.
  Лікар визначає діагноз, робить призначення пацієнту (процедури, ліки, операції), які фіксуються в Лікарняній картці. Призначення може виконати Медсестра (процедури, ліки) або Лікар (будь-яке призначення).
  Пацієнт може бути виписаний з лікарні, при цьому фіксується остаточний діагноз.
  (Опціонально: реалізувати можливість збереження / експорта документа з інформацією про виписку пацієнта).