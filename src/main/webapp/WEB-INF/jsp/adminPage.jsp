<%@ page import="model.entity.Doctor" %>
<%@ page import="java.util.List" %>
<%@ page import="model.entity.Patient" %>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="locale"/>

<html lang="${language}">
<head>
    <link rel="shortcut icon" href="https://cdn4.iconfinder.com/data/icons/hospital-and-medical-v2/512/cross_hospital_medicine_sign_health-512.png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
    <title>Admin Page</title>
    <style>
        body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
            background: linear-gradient(0.25turn, #7cbdda, #c8dbba, #eee7ad);
            background-size: cover;
        }

        main {
            flex: 1 0 auto;
        }

        .col form{
            width:50%;
            margin:1 auto;
        }
        .col2 form{
            width:50%;
            margin:1 auto;
        }

        .buttonHolder{
            text-align: center;
            margin: 2vh 2vh 2vh 2vh;
        }

    </style>
</head>
<body>
<jsp:include page="elements/header.jsp"/>

<main>
    <div class="container">
        <ul class="collection with-header">
            <li class="collection-header" ><h4 style="text-align: center;">Logged as Admin
            </h4>
                <c:choose>
                    <c:when test="${requestScope.bad_getaway == 'emptyDoctor'}">
                        <p class="red-text" style="text-align: center"><fmt:message key="emptyDoctor" /></p>
                    </c:when>
                </c:choose>
            </li>
        </ul>
    </div>

    <div class="container" style="background: white; min-height: auto;">
        <div class="col" style=" display: flex; flex-direction: line; text-align: center">
            <form action="${pageContext.request.contextPath}/admin/registerDoctor">
                <p class="buttonHolder">
                    <button class="btn info" style="background: #2196F3;"><fmt:message key="registerDoctor"/></button>
                </p>
            </form>
            <form action="${pageContext.request.contextPath}/admin/adminDoctorsPage">
                <p class="buttonHolder">
                    <input type="hidden" name="currentDoctorsPage" value="1">
                    <button class="btn info" style="background: #2196F3;"><fmt:message key="doctorsList"/></button>
                </p>
            </form>
        </div>
        <div class="col2" style=" display: flex; flex-direction: line; text-align: center">
            <form action="${pageContext.request.contextPath}/admin/registerPatient">
                <p class="buttonHolder">
                    <button class="btn info" style="background: #2196F3;"><fmt:message key="registerPatient"/></button>
                </p>
            </form>
            <form action="${pageContext.request.contextPath}/admin/adminPatientsPage">
                <p class="buttonHolder">
                    <input type="hidden" name="currentPatientsPage" value="1">
                    <button class="btn info" style="background: #2196F3;"><fmt:message key="patientsList"/></button>
                </p>
            </form>
        </div>
    </div>
</main>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
<script>
    $(document).ready(function () {
        $('select').formSelect();
        $('.tabs').tabs();
        $('.sidenav').sidenav();
        $('.modal').modal();
    });
</script>
</body>
</html>
