<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="locale"/>

<html lang="${language}">
<head>
    <link rel="shortcut icon" href="https://cdn4.iconfinder.com/data/icons/hospital-and-medical-v2/512/cross_hospital_medicine_sign_health-512.png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
    <title>Hospital Project</title>
    <style>
        body{
            background: linear-gradient(0.25turn, #7cbdda, #c8dbba, #eee7ad);
            background-size: cover;
        }
    </style>
</head>

<body>
<header>
    <nav style="background: linear-gradient(0.25turn, #4a8faf, #c8dbba, #ccc26b); border-radius: 40px; margin-top: 10px;">
        <div class="container">
            <div class="nav-wrapper">
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
                <a href="${pageContext.request.contextPath}/login" class="brand-logo"><i class="material-symbols-outlined">local_hospital</i><fmt:message key="hospital" /></a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li>
                        <div>
                            <form>
                                <select id="language" name="language" onchange="submit()">
                                    <option  value="en" ${language == 'en' ? 'selected' : ''}>English</option>
                                    <option  value="ua" ${language == 'ua' ? 'selected' : ''}>Українська</option>
                                </select>
                            </form>
                        </div>
                    </li>
                    <li><a href="${pageContext.request.contextPath}/register" class="btn cyan darken-2"><fmt:message key="register" /></a></li>
                    <li><a href="${pageContext.request.contextPath}/login" class="btn cyan darken-2"><fmt:message key="login" /></a></li>
                </ul>
            </div>
        </div>
    </nav>

    <ul class="sidenav" id="mobile-demo">
        <li><a href="${pageContext.request.contextPath}/"><fmt:message key="home" /></a></li>
    </ul>
</header>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
<script>
    $(document).ready(function () {
        $('.sidenav').sidenav();
        $('.materialboxed').materialbox();
        $('select').formSelect();
    });
</script>
</body>
</html>